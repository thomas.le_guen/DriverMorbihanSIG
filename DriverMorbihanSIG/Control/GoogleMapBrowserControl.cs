﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using Awesomium.Core;
using Awesomium.Windows.Controls;
using DriverMorbihanSIG.Controller;
using DriverMorbihanSIG.Entity;
using DriverMorbihanSIG.EventArgs;
using DriverMorbihanSIG.ExtensionMethods;
using Microsoft.Kinect;
using Microsoft.Kinect.Input;
using Microsoft.Kinect.Toolkit.Input;
using Microsoft.Kinect.Wpf.Controls;
using Newtonsoft.Json;

namespace DriverMorbihanSIG.Control
{
    /// <summary>
    /// Class extending the Awesomium WebControl in order to add the IKinectControl interface.
    /// This allows for recognition as manipulatable by the KinectRegion default class.
    /// Therefore, the WebBrowser becomes a ManipulatableBrowser
    /// </summary>
    internal class GoogleMapBrowserControl : WebControl, IKinectControl
    {
        /// <summary>
        /// Initial coordinates used in order to reset the map
        /// </summary>
        public Coordinates InitialCoordinates { get; set; }

        /// <summary>
        /// Opened marker to close when an other "tap" event occurs
        /// </summary>
        private Marker _openedMarker;

        /// <summary>
        /// Bounds to calculate the tapped point in latitude and longitude
        /// </summary>
        public Area MapBounds { get; set; }

        /// <summary>
        ///     Current coordinates where the map is currently centered
        /// </summary>
        public Coordinates Coordinates { get; set; }

        /// <summary>
        /// List of markers currently displayed
        /// </summary>
        public List<Marker> Markers { get; set; }

        /// <summary>
        /// Pixel height of the window
        /// </summary>
        public double PixelHeight { get; set; }

        /// <summary>
        /// Pixel width of the window
        /// </summary>
        public double PixelWidth { get; set; }

        /// <summary>
        /// IKinect control implemented method which builds and assign the right controller to the KinectControl
        /// </summary>
        /// <param name="inputModel">IInputModel</param>
        /// <param name="kinectRegion">KinectRegion</param>
        /// <returns>IKinectController</returns>
        public IKinectController CreateController(IInputModel inputModel, KinectRegion kinectRegion)
        {
            var browserController = new GoogleMapBrowserController(this, (ManipulatableModel) inputModel, kinectRegion);
            PixelHeight = GetPixelHeight();
            PixelWidth = GetPixelWidth();
            MapBounds = new Area();
            Coordinates = new Coordinates
            {
                Zoom = 10
            };
            Coordinates.ZoomChanged += SetZoom;
            SetValue(KinectRegion.KinectControllerProperty, browserController);
            Markers = new List<Marker>();
            return browserController;
        }


        /// <summary>
        /// Event listener for zoom management
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SetZoom(object sender, ZoomChangedEventArgs e)
        {
            ExecuteJavascript($"map.setZoom({e.NewZoom})");
        }

        /// <summary>
        /// IKinectControl boolean value to allow the press action
        /// </summary>
        public bool IsPressable => true;

        /// <summary>
        /// IKinectControl boolean value to allow the manipulation action
        /// </summary>
        public bool IsManipulatable => true;

        public void Tap(Area area)
        {
            var marker = Markers.FirstOrDefault(x => x.Coordinates.InRange(area));
            CloseInfoWindow();
            
            if (marker != null)
            {
                OpenInfoWindow(marker.MarkerInfo.Name);
            }
        }

        /// <summary>
        /// Function called to open the information window with the specific marker name as a key
        /// </summary>
        /// <param name="markerName"></param>
        private void OpenInfoWindow(string markerName)
        {
            _openedMarker = Markers.Find(m => m.MarkerInfo.Name == markerName);
            ExecuteJavascript($"openInfoWindow('{markerName.Replace("'",@"\'")}')");
        }

        /// <summary>
        /// Closes the opened marker stored in a private var
        /// </summary>
        public void CloseInfoWindow()
        {
            if (_openedMarker != null)
                ExecuteJavascript($"closeInfoWindow('{_openedMarker.MarkerInfo.Name.Replace("'", @"\'")}')");
        }

        /// <summary>
        /// Builds a dictionary with the current bounds (made of 2 coordinates : NorthEast and SouthWest).
        /// We are calling the native google map functions.
        /// This method is used for converting (x,y) values (where 0 > x, y > 1) to (lat,lng)
        /// </summary>
        /// <returns>The SouthWest and NorthEast coordinates</returns>
        public void RefreshBounds()
        {
            var jsObjectBounds = (JSObject)ExecuteJavascriptWithResult("map.getBounds()");
            var northEastJs = (JSObject)jsObjectBounds.Invoke("getNorthEast");
            var southWestJs = (JSObject)jsObjectBounds.Invoke("getSouthWest");

            MapBounds.Maximum = new Coordinates
            {
                Latitude = (double) northEastJs.Invoke("lat"),
                Longitude = (double) northEastJs.Invoke("lng")
            };

            MapBounds.Minimum = new Coordinates
            {
                Latitude = (double) southWestJs.Invoke("lat"),
                Longitude = (double) southWestJs.Invoke("lng")
            };
        }

        /// <summary>
        /// Pans the map by the pixel values specified
        /// </summary>
        /// <param name="pixels">String containing pixel values</param>
        public void PanBy(string pixels)
        {
            ExecuteJavascript($"map.panBy({pixels})");
        }

        /// <summary>
        /// Returns the double value of the pixel height from the browser by a javascript call
        /// </summary>
        /// <returns>PixelHeight</returns>
        public double GetPixelHeight()
        {
            return (double) ExecuteJavascriptWithResult("window.innerHeight");
        }

        /// <summary>
        /// Returns the double value of the pixel width from the browser by a javascript call
        /// </summary>
        /// <returns>PixelWidth</returns>
        public double GetPixelWidth()
        {
            return (double) ExecuteJavascriptWithResult("window.innerWidth");
        }

        /// <summary>
        /// Resets the map to the initial coordinates
        /// </summary>
        public void ResetMap()
        {
            Coordinates.Zoom = InitialCoordinates.Zoom;
            PanTo(InitialCoordinates);
            ExecuteJavascript("removeMarkers()");
        }

        public void PanTo(Coordinates c)
        {
            ExecuteJavascript($"map.panTo({c})");
        }

        /// <summary>
        /// Function opening the current markers
        /// </summary>
        /// <param name="category"></param>
        public void ShowMarkers()
        {
            ExecuteJavascriptWithResult($"showMarkers({JsonConvert.SerializeObject(Markers)})");
        }

        /// <summary>
        /// Concatenates the KinectGestureSet
        /// </summary>
        /// <returns></returns>
        public KinectGestureSettings GetKinectGestureSettings()
        {
            return (KinectRegion.GetIsScrollInertiaEnabled(this)
                       ? KinectGestureSettings.ManipulationTranslateInertia
                       : 0) |
                   (KinectRegion.GetIsHorizontalRailEnabled(this)
                       ? KinectGestureSettings.ManipulationTranslateRailsX
                       : 0) |
                   (KinectRegion.GetIsVerticalRailEnabled(this) ? KinectGestureSettings.ManipulationTranslateRailsY : 0) |
                   (KinectRegion.GetZoomMode(this) == ZoomMode.Enabled ? KinectGestureSettings.ManipulationScale : 0);
        }
    }
}