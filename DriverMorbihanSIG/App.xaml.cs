﻿using System.Windows;
using Microsoft.Kinect.Wpf.Controls;

namespace DriverMorbihanSIG
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal KinectRegion KinectRegion { get; set; }
    }
}