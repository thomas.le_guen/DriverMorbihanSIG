﻿var map;
var markers = [];
var infoWindows = [];

function openInfoWindow(markerName) {
    infoWindows[markerName].open(map, markers[markerName]);
}

function closeInfoWindow(markerName) {
    infoWindows[markerName].close();
}

function getContentStringFromRawMarker(marker)
{
    return marker.data.name +
        "<br>Adresse :  " +
        marker.data.address +
        "<br>Ville : " +
        marker.data.zip +
        " " +
        marker.data.city +
        "<br>Téléphone : " +
        marker.data.phone;
}

function showMarkers(markersRaw) {
    markersRaw.forEach(function (element) {
        markers[element.data.name] = new google.maps.Marker({
            position: new google.maps.LatLng(element.lat, element.lng),
            map: map,
            title: element.data.name
        });

        infoWindows[element.data.name] = new google.maps.InfoWindow({
            content: getContentStringFromRawMarker(element)
        });
    });
}

function removeMarkers() {
    for (var key in markers) {
        if (markers.hasOwnProperty(key)) {
            markers[key].setMap(null);
        }
    }
    markers = [];
}

function initMap() {
    var vannes = new google.maps.LatLng(47.6667, -2.75);

    map = new google.maps.Map(document.getElementById("map"), {
        center: vannes,
        mapTypeId: 'hybrid',
        zoom: 10
    });
}