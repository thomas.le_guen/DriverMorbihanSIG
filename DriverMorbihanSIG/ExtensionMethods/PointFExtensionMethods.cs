﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DriverMorbihanSIG.Entity;
using Microsoft.Kinect;

namespace DriverMorbihanSIG.ExtensionMethods
{
    public static class PointFExtensionMethods
    {
        public static PointF ToPixels(this PointF p, double height, double width)
        {
            return new PointF
            {
                X = p.X * (float) width,
                Y = p.Y * (float) height
            };
        }

        public static string ToStringWithPoint(this PointF p)
        {
            double x = p.X;
            double y = p.Y;
            return $"{x.ToStringWithPoint()},{y.ToStringWithPoint()}";
        }

        public static PointF AdditevelyInverted(this PointF p)
        {
            return new PointF
            {
                X = -p.X,
                Y = -p.Y
            };
        }

        public static double RelativeInvertedY(this PointF p, MainWindow m, FrameworkElement e)
        {
            var elementRelativeHeight = m.GetRelativeHeight(e);
            var relativeY = (p.Y - elementRelativeHeight.AdditivelyInverted()) / elementRelativeHeight;
            return relativeY.AdditivelyInverted();
        }

        public static Coordinates ToCoordinates(this PointF p, Area area)
        {
            return new Coordinates
            {
                Latitude = area.Minimum.Latitude + p.Y * area.LatitudeRange,
                Longitude = area.Minimum.Longitude + p.X * area.LongitudeRange
            };
        }
    }
}
