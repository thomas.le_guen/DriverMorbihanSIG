﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriverMorbihanSIG.ExtensionMethods
{
    public static class DoubleExtensionMethods
    {
        public static string ToStringWithPoint(this double d)
        {
            var numberWithPoint = new NumberFormatInfo
            {
                NumberDecimalSeparator = "."
            };

            return d.ToString(numberWithPoint);
        }

        public static bool InRangeClosed(this double d, double min, double max)
        {
            return d >= min && d <= max;
        }

        public static double AdditivelyInverted(this double d)
        {
            return 1 - d;
        }
    }
}
