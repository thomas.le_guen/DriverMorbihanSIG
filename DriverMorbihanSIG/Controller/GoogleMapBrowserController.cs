﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Policy;
using System.Windows;
using DriverMorbihanSIG.Control;
using DriverMorbihanSIG.Entity;
using DriverMorbihanSIG.ExtensionMethods;
using Microsoft.Kinect;
using Microsoft.Kinect.Input;
using Microsoft.Kinect.Toolkit.Input;
using Microsoft.Kinect.Wpf.Controls;

namespace DriverMorbihanSIG.Controller
{
    /// <summary>
    ///     Controller managing events of both Manipulatable and Pressable models on a browser object
    /// </summary>
    internal class GoogleMapBrowserController : IKinectManipulatableController, IKinectPressableController
    {
        #region Properties

        #region Framework related properties

        /// <summary>
        ///     Framework Element managed by the controller
        /// </summary>
        public FrameworkElement Element { get; private set; }

        /// <summary>
        ///     Manipulatable input model managing the drag/drop interactions
        /// </summary>
        public ManipulatableModel ManipulatableInputModel { get; private set; }

        /// <summary>
        ///     Pressable input model managing the on press interactions
        /// </summary>
        public PressableModel PressableInputModel { get; private set; }

        /// <summary>
        ///     Reference to the parent KinectRegion orchestrating the whole
        /// </summary>
        protected KinectRegion KinectRegion { get; private set; }

        #endregion

        #region Local properties & lambdas

        /// <summary>
        ///     Function converting x or y to latitude or longitude based on the range and the minimum coordinate of the map
        ///     displayed
        /// </summary>

        private GoogleMapBrowserControl _browser;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor assigning properties and initalizing InputModels.
        /// </summary>
        /// <param name="element">wpf element</param>
        /// <param name="manipulatableModel">manipulatable model</param>
        /// <param name="kinectRegion">kinect region</param>
        public GoogleMapBrowserController(FrameworkElement element, ManipulatableModel manipulatableModel,
            KinectRegion kinectRegion)
        {
            Element = element;
            KinectRegion = kinectRegion;
            AssignManipulatableEventMethods(manipulatableModel);
            AssignPressableEventMethods(element);
            _browser = (GoogleMapBrowserControl) element;
        }

        #endregion

        #region Clean-up

        /// <summary>
        ///     Clean-up method
        /// </summary>
        public void Dispose()
        {
            DisposeOfManipulatableModel();
            DisposeOfPressableModel();
            Element = null;
            KinectRegion = null;
        }

        #endregion

        #region Pressable event method

        /// <summary>
        ///     Tapping triggers panning to a specific destination on the google map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTapped(object sender, KinectTappedEventArgs e)
        {
            _browser.RefreshBounds();
            var area = GetAreaFromWindowCoordinates(e.Position);
            _browser.Tap(area);
        }

        #endregion

        #region Manipulatable event methods 

        /// <summary>
        ///     Main method managing the position of the cursor while dragging the element
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnManipulationUpdated(object sender, KinectManipulationUpdatedEventArgs e)
        {
            _browser.PanBy(
                e.Delta
                    .Translation
                    .AdditevelyInverted()
                    .ToPixels(_browser.PixelHeight, _browser.PixelWidth)
                    .ToStringWithPoint()
            );
        }

        #endregion

        #region Utils / Breathing room

        /// <summary>
        ///     Builds an area object which contains max an min coordinates (south-west - north-east)
        /// </summary>
        /// <param name="p">PointF</param>
        /// <returns>An Area</returns>
        public Area GetAreaFromWindowCoordinates(PointF p)
        {
            var area = new Area();
            var relativePoint = new PointF
            {
                X = p.X,
                Y = (float) p.RelativeInvertedY((MainWindow) KinectRegion.Parent, Element)
            };

            var latitudeSpread = _browser.MapBounds.LatitudeRange * 0.06f;
            var longitudeSpread = _browser.MapBounds.LongitudeRange * 0.02f;

            area.Maximum = relativePoint.ToCoordinates(_browser.MapBounds);
            area.Maximum.Add(latitudeSpread, longitudeSpread);

            area.Minimum = relativePoint.ToCoordinates(_browser.MapBounds);
            area.Minimum.Add(-latitudeSpread, -longitudeSpread);

            return area;
        }

        /// <summary>
        ///     Initialization of the PressableInputModel
        /// </summary>
        /// <param name="element"></param>
        private void AssignPressableEventMethods(FrameworkElement element)
        {
            PressableInputModel = new PressableModel(element);
            if (PressableInputModel != null)
                PressableInputModel.Tapped += OnTapped;
        }

        /// <summary>
        ///     Initialization of the ManipulatableInputModel
        /// </summary>
        /// <param name="manipulatableModel"></param>
        private void AssignManipulatableEventMethods(ManipulatableModel manipulatableModel)
        {
            if (manipulatableModel != null)
            {
                ManipulatableInputModel = manipulatableModel;
                ManipulatableInputModel.ManipulationUpdated += OnManipulationUpdated;
            }
        }

        /// <summary>
        ///     Cleaning up the ManipulatableInputModel
        /// </summary>
        private void DisposeOfManipulatableModel()
        {
            if (ManipulatableInputModel != null)
            {
                ManipulatableInputModel.ManipulationUpdated -= OnManipulationUpdated;
                ManipulatableInputModel = null;
            }
        }

        /// <summary>
        ///     Cleaning up the PressableInputModel
        /// </summary>
        private void DisposeOfPressableModel()
        {
            if (PressableInputModel != null)
            {
                PressableInputModel.Tapped -= OnTapped;
                PressableInputModel = null;
            }
        }
        #endregion
    }
}