﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriverMorbihanSIG.Entity
{
    public class Area
    {
        public Coordinates Minimum { get; set; }
        public Coordinates Maximum { get; set; }
        public double LatitudeRange => Maximum.Latitude - Minimum.Latitude;
        public double LongitudeRange => Maximum.Longitude - Minimum.Longitude;
    }
}
