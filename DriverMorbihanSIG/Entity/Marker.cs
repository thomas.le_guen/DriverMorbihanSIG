﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DriverMorbihanSIG.Entity
{
    public class Marker
    {
        public Coordinates Coordinates { get; set; }

        [JsonProperty("lat")]
        public string Lat { get; set; }

        [JsonProperty("lng")]
        public string Lng { get; set; }

        [JsonProperty("data")]
        public MarkerInfo MarkerInfo { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        public override string ToString()
        {
            return MarkerInfo.Name;
        }
    }
}
