﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DriverMorbihanSIG.EventArgs;
using DriverMorbihanSIG.ExtensionMethods;
using Microsoft.Kinect;
using Newtonsoft.Json;

namespace DriverMorbihanSIG.Entity
{
    public class Coordinates
    {
        public Coordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public Coordinates()
        {
        }

        public event EventHandler<ZoomChangedEventArgs> ZoomChanged;

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        private int _zoom;

        public int Zoom
        {
            get => _zoom;
            set
            {
                _zoom = value;
                ZoomChanged?.Invoke(this, new ZoomChangedEventArgs { NewZoom = value });
            }
        }

        public override string ToString()
        {
            return $"{{lat: {Latitude.ToStringWithPoint()},lng: {Longitude.ToStringWithPoint()}}}";
        }

        public bool InRange(Area area)
        {
            var isInLatitudeRange = Latitude <= area.Maximum.Latitude &&
                                    Latitude >= area.Minimum.Latitude;
            var isInLongitudeRange = Longitude <= area.Maximum.Longitude &&
                                     Longitude >= area.Minimum.Longitude;
            return isInLatitudeRange && isInLongitudeRange;
        }

        public void Add(double latitude, double longitude)
        {
            Latitude += latitude;
            Longitude += longitude;
        }
    }
}
