﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriverMorbihanSIG.EventArgs
{
    public class ZoomChangedEventArgs : System.EventArgs
    {
        public int LastZoom { get; set; }
        public int NewZoom { get; set; }
    }
}