﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DriverMorbihanSIG.Entity;
using DriverMorbihanSIG.HandPointers;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.Input;
using Microsoft.Kinect.Wpf.Controls;
using Newtonsoft.Json;

namespace DriverMorbihanSIG
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Marker> _allMarkers;

        /// <summary>
        ///     Constructor setting up the app and the KinectFramework
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            SetTestUrl();
            LoadButtons();
            var app = (App) Application.Current;
            app.KinectRegion = MainRegion;
            KinectRegion.SetKinectRegion(this, MainRegion);
            MainRegion.KinectSensor = KinectSensor.GetDefault();
        }


        /// <summary>
        ///     Event method loading the values and setting up the Kinect Framework around the browser element
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KinectRegion_Loaded(object sender, RoutedEventArgs e)
        {
            var handOverHeadManagementModel = new HandOverheadEngagementModel(1);
            MainRegion.SetKinectOnePersonManualEngagement(handOverHeadManagementModel);
            Browser.CreateController(new ManipulatableModel(Browser.GetKinectGestureSettings(), Browser), MainRegion);
        }

        /// <summary>
        ///     Event method loading the initial values for the browser element.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            Browser.InitialCoordinates = new Coordinates(47.667f, -2.75f);
            Browser.InitialCoordinates.Zoom = 10;
        }

        /// <summary>
        ///     Event method triggering the reset action on the map, returning to the initial coordinates and zoom level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetMap(object sender, RoutedEventArgs e)
        {
            Browser.ResetMap();
        }

        /// <summary>
        ///     Small method for testing purposes with a standard google map
        /// </summary>
        private void SetTestUrl()
        {
            var uri = Environment.CurrentDirectory + @"\gmap-sample.html";
            Browser.Source = new Uri(uri);
        }

        /// <summary>
        ///     Util method to get the relative height of a framework element in the grid
        /// </summary>
        /// <param name="element">Element of the framework</param>
        /// <returns>Relative height (betwin 0 and 1)</returns>
        public double GetRelativeHeight(FrameworkElement element)
        {
            var rowIndex = Grid.GetRow(element);
            return MainGrid.RowDefinitions[rowIndex].Height.Value /
                            MainGrid.RowDefinitions.Select(r => r.Height.Value).Sum();
        }


        /// <summary>
        /// Event handler for the zoom button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomMap(object sender, RoutedEventArgs e)
        {
            Browser.Coordinates.Zoom += 1;
        }

        /// <summary>
        /// Event listener for the schrink button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SchrinkMap(object sender, RoutedEventArgs e)
        {
            Browser.Coordinates.Zoom -= 1;
        }

        /// <summary>
        /// Event listener for a category button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowMarkers(object sender, RoutedEventArgs e)
        {
            var tag = ((TextBlock) ((Button) sender).Content).Text;
            Browser.ResetMap();
            Browser.Markers = _allMarkers.Where(marker => marker.Tag == tag).ToList();
            Browser.ShowMarkers();
        }

        /// <summary>
        /// Async method to load buttons from the categories of the markers
        /// </summary>
        async void LoadButtons()
        {
            _allMarkers = await LoadMarkers();
            foreach (var tag in _allMarkers.Select(m => m.Tag).Distinct())
            {
                var text = new TextBlock
                {
                    Text = tag,
                    TextWrapping = TextWrapping.Wrap,
                    Height = Double.NaN
                };
                var button = new Button
                {
                    Content = text,
                    Width = 400
                };
                
                button.Click += ShowMarkers;
                Buttons.Children.Add(button);
            }
        }

        /// <summary>
        /// Async method to load markers
        /// </summary>
        /// <returns></returns>
        async Task<List<Marker>> LoadMarkers()
        {
            var r = new StreamReader("markers.json");
            string json = r.ReadToEnd();
            var allMarkers = JsonConvert.DeserializeObject<List<Marker>>(json);
            allMarkers.ForEach(m => m.Coordinates = new Coordinates
            {
                Latitude = Convert.ToDouble(m.Lat.Replace(".", ",")),
                Longitude = Convert.ToDouble(m.Lng.Replace(".", ","))
            });
            allMarkers.ForEach(m => m.Tag = m.Tag.Split('#')[2]);
            return allMarkers;
        }
    }   
}